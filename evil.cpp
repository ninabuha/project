#include"evil.h"

Evil::Evil(SDL_Renderer* renderer, Sprite* sprite){
    this->sprite = sprite;
    SDL_Surface *surface = IMG_Load("resources/creatures/evil1.png");

    spriteTexture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    // dimenzije sprajta sa slike
    frameRect = new SDL_Rect;
    frameRect->x = 0; frameRect->y = 0;
    frameRect->w = 750; frameRect->h = 680;

    // dimenzije sprajta na prozoru
    spriteRect = new SDL_Rect;
    spriteRect->x = 400; spriteRect->y = 300;
    spriteRect->w = 50; spriteRect->h = 50;
}
void Evil::draw(SDL_Renderer *renderer){
    SDL_RenderCopy(renderer, spriteTexture, frameRect, spriteRect);
}

void Evil::move(int dx, int dy, Labyrinth *labyrinth){
    int spriteX = spriteRect->x;
    int spriteY = spriteRect->y;
    int spriteW = spriteRect->w;
    int spriteH = spriteRect->h;

    int girlX = sprite->getSpriteRect()->x;
    int girlY = sprite->getSpriteRect()->y;
    int girlW = sprite->getSpriteRect()->w;
    int girlH = sprite->getSpriteRect()->h;

    // kolizija sa sprajtom
    if(spriteX + dx <= girlX+girlW && spriteX+spriteW + dx >= girlX && spriteY + dy <= girlY+girlH && spriteY+spriteH + dy >= girlY){
        sprite->setHealth(0);
        eaten = 1;
    }

    if(spriteX >= 640-50){
        randomDirection = 1;
    }
    else if(spriteX <= 0){
        randomDirection = 2;
    }
    else if(spriteY >= 480-50){
        randomDirection = 3;
    }
    else if(spriteY <= 0){
        randomDirection = 4;
    }

    spriteRect->x += dx;
    spriteRect->y += dy;

    if(changeDirection == 200){
        cout<<randomDirection;
        randomDirection = rand() % 4+1;
        changeDirection = 0;

    }

    changeDirection += 1;
}

void Evil::move(Labyrinth *labyrinth){
    if(randomDirection == 1) {
        move(-1, 0, labyrinth);
    }
    if(randomDirection == 2) {
        move(1, 0, labyrinth);
    }
    if(randomDirection == 3) {
        move(0, -1, labyrinth);
    }
    if(randomDirection == 4) {
        move(0, 1, labyrinth);
    }
}
