#ifndef LABYRINTH_H_INCLUDED
#define LABYRINTH_H_INCLUDED

#include<vector>

#include <fstream>
#include <string>
#include <iostream>

#include "drawable.h"

using namespace std;

class Labyrinth : public Drawable{
private:
    vector<SDL_Rect*> labyrinth;
public:
    Labyrinth(istream &inputStream);

    vector<SDL_Rect*> getLabyrinth(){ return labyrinth; }
    void addWall(SDL_Rect*);
    void draw(SDL_Renderer*);
};

#endif // LABYRINTH_H_INCLUDED
