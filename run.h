#ifndef RUN_H_INCLUDED
#define RUN_H_INCLUDED

#include <fstream>
#include <string>
#include <iostream>
#include <sstream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "drawable.h"
#include "movable.h"
#include "sprite.h"
#include "labyrinth.h"
#include "finish.h"
#include "hint.h"
#include"evil.h"

using namespace std;

class Run{
private:
    vector<Drawable*> drawables;
    vector<Movable*> movables;

    vector<Hint*> hints;
    SDL_Window *window;
    SDL_Renderer *renderer;

    bool endGame = false;

public:
    Run(string title);
    bool getEndGame(){ return endGame; }
    void setEndGame(bool endGame) {this->endGame = endGame;}
    void addDrawable(Drawable*);
    void addMovable(Movable*);
    void createHints(istream&);
    void run();

    virtual ~Run();
};

#endif // RUN_H_INCLUDED
