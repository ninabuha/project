#include <iostream>
#include "run.h"

using namespace std;

int main(int argc, char** args)
{
    Run *run = new Run("Projekat");
    run->run();

    delete run;
    return 0;
}
