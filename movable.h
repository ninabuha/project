#ifndef MOVABLE_H_INCLUDED
#define MOVABLE_H_INCLUDED

#include"labyrinth.h"

// interfejs
class Movable{
public:
    // kretanje objekata
    virtual void move(Labyrinth*)=0;
    virtual void move(int dx, int dy, Labyrinth*)=0;
};

#endif // MOVABLE_H_INCLUDED
