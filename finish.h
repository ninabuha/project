#ifndef FINISH_H_INCLUDED
#define FINISH_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <fstream>
#include <string>
#include <iostream>

#include "drawable.h"

class Finish : public Drawable{
private:
    SDL_Rect *coinRect;
    SDL_Texture *coinTexture;

    int frameWidth;
    int frameHeight;
    int frameCount;
    int currentFrame;
    int totalFrames = 0;
    SDL_Rect *frameRect;

public:
    char state;
    Finish(SDL_Renderer *renderer){
        currentFrame = 0;
        totalFrames = 0;

        SDL_Surface *surface = IMG_Load("resources/creatures/coin.png");
        //broj frejmova sa slike
        frameCount = surface->w/100;
        coinTexture = SDL_CreateTextureFromSurface(renderer, surface);
        SDL_FreeSurface(surface);

        //dimenzije sprajta sa slike
        frameRect = new SDL_Rect;
        frameRect->x = 0; frameRect->y = 0;
        frameRect->w = 100; frameRect->h = 100;

        //dimenzije sprajta na prozoru
        coinRect = new SDL_Rect;
        coinRect->x = 580; coinRect->y = 100;
        coinRect->w = 40; coinRect->h = 40;
    }

    void draw(SDL_Renderer *renderer){
        //dobija se pocetka trenutniog frejma
        frameRect->x = currentFrame * frameRect->w;
        //stavlja sliku na mesto namenjeno za sprajt
        SDL_RenderCopy(renderer, coinTexture, frameRect, coinRect);
        totalFrames++;

        //odredjuje kada se menja frejm
        if(totalFrames % 10 == 0){ //na svakih 10px se menja frejm
            currentFrame++;

            //ako je trenutni frejm veci ili jednak uk broju frejmova
            if(currentFrame >= frameCount){
                currentFrame = 0;
            }
            totalFrames = 0;
        }
    }

};

#endif // FINISH_H_INCLUDED
