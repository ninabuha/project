#ifndef HINT_H_INCLUDED
#define HINT_H_INCLUDED

#include<vector>

#include <fstream>
#include <string>
#include <iostream>

#include "drawable.h"

class Hint : public Drawable{
private:
    char hintType;
    int x;
    int y;
public:
    Hint(SDL_Renderer*, char, int, int);
    void draw(SDL_Renderer*);
};

#endif // HINT_H_INCLUDED
