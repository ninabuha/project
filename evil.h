#ifndef EVIL_H_INCLUDED
#define EVIL_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include"sprite.h"

#include <stdlib.h>

class Evil : public Drawable, public Movable{
private:
    Sprite* sprite;
    SDL_Rect *frameRect;
    SDL_Rect *spriteRect;
    SDL_Texture *spriteTexture;
    int changeDirection = 0;
    int randomDirection = 1;
    int eaten = 0;
public:
    Evil(SDL_Renderer*, Sprite*);

    SDL_Rect *getSpriteRect(){ return spriteRect ;}
    int getEaten() { return eaten; }
    void draw(SDL_Renderer*);
    void move(Labyrinth*);
    void move(int, int, Labyrinth*);
};

#endif // EVIL_H_INCLUDED
