#include "sprite.h"


Sprite::Sprite(SDL_Renderer *renderer) : Drawable(), Movable(){
    SDL_Surface *surface = IMG_Load("resources/creatures/girl.png");

    frameCount = surface->w/600;
    spriteTexture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    // dimenzije sprajta sa slike
    frameRect = new SDL_Rect;
    frameRect->x = 20; frameRect->y = 0;
    frameRect->w = 600; frameRect->h = 750;

    // dimenzije sprajta na prozoru
    spriteRect = new SDL_Rect;
    spriteRect->x = 10; spriteRect->y = 300;
    spriteRect->w = 30; spriteRect->h = 40;
}

void Sprite::draw(SDL_Renderer *renderer){
    //dobija se pocetka trenutniog frejma
    frameRect->x = currentFrame * frameRect->w;
    //stavlja sliku na mesto namenjeno za sprajt
    SDL_RenderCopy(renderer, spriteTexture, frameRect, spriteRect);
    totalFrames++;

    //odredjuje da se frejm menja na svakih 10px
    if(totalFrames % 10 == 0){
        currentFrame++;

        //ako je trenutni frejm veci ili jednak uk broju frejmova, trenutni frejm se postavlja na pocetni
        if(currentFrame >= frameCount){
            currentFrame = 0;
        }
        totalFrames = 0;
    }
}

void Sprite::move(int dx, int dy, Labyrinth *labyrinth){
    int spriteX = spriteRect->x;
    int spriteY = spriteRect->y;
    int spriteW = spriteRect->w;
    int spriteH = spriteRect->h;

    // ukoliko sprajt stigne do cilja
    if(spriteX <= 580+45 && spriteX+spriteW >= 580 && spriteY <= 100+40 && spriteY+spriteH >= 100){
        endGame = true;
    }

    for(size_t i = 0; i < labyrinth->getLabyrinth().size(); i++){
        int x = labyrinth->getLabyrinth()[i]->x;
        int y = labyrinth->getLabyrinth()[i]->y;
        int w = labyrinth->getLabyrinth()[i]->w;
        int h = labyrinth->getLabyrinth()[i]->h;


        // kolizija sa zidovima lavirinta
        if(spriteX + dx+dx <= x+w && spriteX+spriteW + dx+dx >= x && spriteY + dy+dy <= y+h && spriteY+spriteH + dy+dy >= y){
            kolizija = true;
            health -= 1;

            /*if(lastDirection == 1){
                spriteRect->x += 1;
            }
            if(lastDirection == 2){
                spriteRect->x -= 1;
            }
            if(lastDirection == 4){
                spriteRect->y += 1;
            }
            if(lastDirection == 8){
                spriteRect->y -= 1;
            }*/
        }
        /*else{
            spriteRect->x += dx;
            spriteRect->y += dy;
        }*/

    }
    if(!kolizija){
        spriteRect->x += dx;
        spriteRect->y += dy;
    }
}

void Sprite::move(Labyrinth *labyrinth){
    if(state != 0) {
        // ukoliko nema kolizije, sprajt je u mogcnosti da se krece
        if(state & 1 && !kolizija) {
            //lastDirection = 1;
            move(-1, 0, labyrinth);
        }
        if(state & 2 && !kolizija) {
            //lastDirection = 2;
            move(1, 0, labyrinth);
        }
        if(state & 4 && !kolizija) {
            //lastDirection = 4;
            move(0, -1, labyrinth);
        }
        if(state & 8 && !kolizija) {
            //lastDirection = 8;
            move(0, 1, labyrinth);
        }
    }
    else if(state == 0){
        //lastDirection = 0;
        currentFrame = 0;
    }
}
