#include "run.h"
Run::Run(string title){
    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_PNG);
    window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_RESIZABLE);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

void Run::addDrawable(Drawable *drawable) {
    drawables.push_back(drawable);
}
void Run::addMovable(Movable *movable) {
    movables.push_back(movable);
}

void Run::createHints(istream &inputStream){
    while(!inputStream.eof()) {
        char hintType;
        int x, y;

        inputStream >> hintType >> x >> y;
        Hint *hint = new Hint(renderer, hintType, x, y);

        addDrawable(hint);
    }
}

void Run::run(){
    int maxDelay = 10;
    int frameStart = 0;
    int frameEnd = 0;

    bool running = true;
    SDL_Event event;


    Sprite *girl = new Sprite(renderer);
    Evil *evil = new Evil(renderer, girl);
    Evil *evil1 = new Evil(renderer, girl);
    Finish *finish = new Finish(renderer);


    ifstream path("resources/labyrinth/coordinates.txt");
    Labyrinth *labyrinth = new Labyrinth(path);

    // kreiranje hintova
    ifstream pathHints("resources/hints.txt");
    createHints(pathHints);

    addDrawable(finish);
    addDrawable(girl);
    addDrawable(evil);
    addDrawable(evil1);
    //addDrawable(labyrinth);
    addMovable(girl);
    addMovable(evil);
    addMovable(evil1);


    while(running) {
        frameStart = SDL_GetTicks();
        while(SDL_PollEvent(&event)) {
            switch(event.type){
                    case SDL_QUIT:
                        running = false;
                        break;
                    case SDL_KEYDOWN:
                        switch(event.key.keysym.sym){
                            case SDLK_RIGHT:
                                girl->state |= Sprite::RIGHT;
                                break;
                            case SDLK_LEFT:
                                girl->state |= Sprite::LEFT;
                                break;
                            case SDLK_UP:
                                girl->state |= Sprite::UP;
                                break;
                            case SDLK_DOWN:
                                girl->state |= Sprite::DOWN;
                                break;
                        }
                        break;
                    case SDL_KEYUP:
                        switch(event.key.keysym.sym){
                            case SDLK_RIGHT:
                                girl->state &= ~Sprite::RIGHT;
                                break;
                            case SDLK_LEFT:
                                girl->state &= ~Sprite::LEFT;
                                break;
                            case SDLK_UP:
                                girl->state &= ~Sprite::UP;
                                break;
                            case SDLK_DOWN:
                                girl->state &= ~Sprite::DOWN;
                                break;
                        }
                        break;
                }

            }


        for(size_t i = 0; i < movables.size(); i++) {
            movables[i]->move(labyrinth);
        }

        // boja pozadine
        if(girl->getKolizija()){
            SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
            SDL_RenderClear(renderer);
        }
        else if(!girl->getKolizija()){
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            SDL_RenderClear(renderer);
        }

        girl->setKolizija(false);

        for(size_t i = 0; i < drawables.size(); i++) {
            drawables[i]->draw(renderer);
        }


        // ispisivanje health-a
        SDL_Color color = {255, 0, 0};

        stringstream ss;
        ss << "Health: " << girl->getHealth();

        int ret = TTF_Init();
        TTF_Font* font = TTF_OpenFont("lazy.ttf", 24);

        SDL_Surface* sm = TTF_RenderText_Solid(font, ss.str().c_str(), color);
        SDL_Texture* message = SDL_CreateTextureFromSurface(renderer, sm);

        SDL_Rect message_box;
          message_box.x = 10;
          message_box.y = 10;
          message_box.w = sm->w;
          message_box.h = sm->h;
        SDL_RenderCopy(renderer, message, NULL, &message_box);


        // ukoliko je health jednak nuli ili je player stigao do cilja, igra se zaustavlja
        if(girl->getHealth() == 0 || girl->getEndGame() == 1){
            SDL_Color color1 = {0, 0, 0};
            stringstream sss;
            if(evil->getEaten() == 1){
                color1 = {255, 0, 0};
                sss << "You've died!";
            }
            else if(girl->getHealth() == 0){
                color1 = {0, 0, 0};
                sss << "You are dead!";
            }
            else{
                color1 = {255, 255, 255};
                sss << "You've won!";
            }

            int ret1 = TTF_Init();
            TTF_Font* font = TTF_OpenFont("lazy.ttf", 90);

            SDL_Surface* sm1 = TTF_RenderText_Solid(font, sss.str().c_str(), color1);
            SDL_Texture* mesage1 = SDL_CreateTextureFromSurface(renderer, sm1);

            SDL_Rect message_box1;
            if(girl->getEndGame() == 1){
                message_box1.x = 80;
            }
            else{
                message_box1.x = 20;
            }

              message_box1.y = 180;
              message_box1.w = sm1->w;
              message_box1.h = sm1->h;
            SDL_RenderCopy(renderer, mesage1, NULL, &message_box1);

            running = false;
        }




        SDL_RenderPresent(renderer);

        if(running == false){
            SDL_Delay(3000);
        }

        frameEnd = SDL_GetTicks();
        if(frameEnd - frameStart < maxDelay) {
            SDL_Delay(maxDelay - (frameEnd - frameStart));
        }
    }
}

Run::~Run(){
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
