#include "labyrinth.h"

Labyrinth::Labyrinth(istream &inputStream) : Drawable(){
    int x, y, w, h;

    while(!inputStream.eof()) {
        inputStream >> x >> y >> w >> h;
        SDL_Rect *wall = new SDL_Rect;
        wall->x = x;
        wall->y = y;
        wall->w = w;
        wall->h = h;

        addWall(wall);
    }
}

void Labyrinth::addWall(SDL_Rect *wall){
    labyrinth.push_back(wall);
}

void Labyrinth::draw(SDL_Renderer *renderer){
    for(size_t i = 0; i < labyrinth.size(); i++){
        SDL_SetRenderDrawColor(renderer, 100, 0, 0, 255);
        SDL_RenderFillRect(renderer, labyrinth[i]);
    }
}
