#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <fstream>
#include <string>
#include <iostream>

#include "drawable.h"
#include "movable.h"
#include "labyrinth.h"

using namespace std;

class Sprite : public Drawable, public Movable{
public:
    enum moving{STOP=0, LEFT=1, RIGHT=2, UP=4, DOWN=8};

private:
    SDL_Rect *frameRect;
    int frameCount;
    int currentFrame = 0;
    int totalFrames = 0;

    SDL_Rect *spriteRect;
    SDL_Texture *spriteTexture;

    bool endGame = false;
    bool kolizija = false;
    int health = 1000;

    int lastDirection = 0;

public:
    SDL_Rect *getSpriteRect(){ return spriteRect ;}
    bool getKolizija(){ return kolizija ;}
    void setKolizija(bool kolizija){ this->kolizija = kolizija ;}
    int getHealth(){ return health ;}
    int setHealth(int health){ this->health = health ;}
    int getEndGame(){ return endGame ;}
    int state = 0;

    Sprite(SDL_Renderer*);
    virtual void draw(SDL_Renderer*);
    virtual void move(Labyrinth*);
    virtual void move(int, int, Labyrinth*);

};


#endif // SPRITE_H_INCLUDED
