#include "hint.h"

Hint::Hint(SDL_Renderer *renderer, char hintType, int x, int y) : Drawable(){
    this->hintType = hintType;
    this->x = x;
    this->y = y;
}

void Hint::draw(SDL_Renderer *renderer){

    if(hintType == 'f'){
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);

    }
    if(hintType == 'h'){
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

    }
    SDL_RenderDrawPoint(renderer, x, y);

}
